<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>お墓・霊園・墓地の情報数No.1！全国のお墓が探せる【いいお墓】</title>
    <meta name="description" content="お墓・霊園・墓地探しに全国6800以上の物件を紹介する日本一のお墓の総合サイト。霊園・墓地の資料請求や来園予約、お墓の選び方相談を無料で受付。費用や相場・墓石見積・建墓・改葬などお墓に関する情報が満載">
    <meta name="keywords" content="霊園,墓地,墓園,お墓">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, maximum-scale=1.0">
    <meta property="og:title" content="お墓・霊園・墓地の情報数No.1！全国のお墓が探せる【いいお墓】">
    <meta property="og:description" content="お墓・霊園・墓地探しに全国6800以上の物件を紹介する日本一のお墓の総合サイト">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://www.e-ohaka.com/sp/">
    <meta property="og:image" content="https://www.e-ohaka.com/sp/common_img/og-image.jpg">
    <meta property="og:site_name" content="全国のお墓が探せる「いいお墓」">
    <link rel="canonical" href="https://www.e-ohaka.com/">
    <link rel="apple-touch-icon" href="https://www.e-ohaka.com/sp/img/webclip.png">
    <link rel="shortcut icon" href="https://www.e-ohaka.com/sp/img/webclip.png">
    {!! Html::style('assets/css/default.css') !!}
    {!! Html::style('assets/css/common.css') !!}
    {!! Html::style('assets/css/style_150212.css') !!}
    {!! Html::style('assets/css/top.css') !!}
    {!! Html::style('assets/css/jquery.sidr.bare.css') !!}
    {!! Html::style('assets/css/modal.css') !!}
</head>