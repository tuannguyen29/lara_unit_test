@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>To Do List [Laravel 5.5] </h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('task.create') }}"> Create New To Do</a>
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Title</th>
            <th>Date Created</th>
            <th>Last Updated</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($tasks as $task)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $task->name}}</td>
                <td>{{ $task->created_at}}</td>
                <td>{{ $task->updated_at}}</td>
                <td>
                    <a class="btn btn-info" href="{{ route('task.show', $task->id) }}">Show</a>
                    <a class="btn btn-primary" href="{{ route('task.edit', $task->id) }}">Edit</a>
                    {!! Form::open(['method' => 'DELETE','route' => ['task.destroy', $task->id],'style'=>'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>


    {!! $tasks->links() !!}
</div>
@endsection
