$(function(){
    /*
    var openBtn = $('#open_btn'), closeBtn = $('#close_btn'),
    // ↑ '#open_btn'にオープンボタンID、'#close_btn'にクローズボタンIDを指定
    modalBox = $('#modalbox'),
    // ↑ モーダルウィンドウで表示する要素のIDを指定
    layer = $('<div />').appendTo('body').addClass('layer');
      
    openBtn.on("click", function(){
        modalBox.css({
	    	top: ($(window).height() - modalBox.outerHeight()) / 2,
	        left: ($(window).width() - modalBox.outerWidth()) / 2
		});
		$([modalBox[0],layer[0]]).fadeIn(300);
    });
    $([layer[0],closeBtn[0]]).on("click", function(){
        $([modalBox[0],layer[0]]).fadeOut(300);
    });
	*/
	var modalBox = $('#modalbox');

	$(window).resize(function () {
		modalBox.css({
        	top: ($(window).height() - modalBox.outerHeight()) / 2,
			left: ($(window).width() - modalBox.outerWidth()) / 2
    	});
    });
	
});

//body固定関数
var bodyElm = $('body');
var scrollPosi;
function bodyFix() {
	scrollPosi = $(window).scrollTop();
	bodyElm.css({
		'position': 'fixed',
		'width': '100%',
		'z-index': '1',
		'top': -scrollPosi
	});
}

//body fixリセット
function bodyFixReset() {
	bodyElm.css({
		'position': 'relative',
		'width': 'auto',
		'top':'auto'
	});
	//scroll位置を調整
	$("html, body").scrollTop(scrollPosi);
}

function clickOpenSendOpen(){
	modalBox = $('#modalbox'),
    // ↑ モーダルウィンドウで表示する要素のIDを指定
    layer = $('<div />').appendTo('body').addClass('layer');

	modalBox.css({
		top: ($(window).height() - modalBox.outerHeight()) / 2,
		left: ($(window).width() - modalBox.outerWidth()) / 2
	});

	$([modalBox[0],layer[0]]).fadeIn(300);
}

function clickOpenSendClose(){
    modalBox = $('#modalbox'),
    // ↑ モーダルウィンドウで表示する要素のIDを指定

	modalBox.css({
		top: ($(window).height() - modalBox.outerHeight()) / 2,
		left: ($(window).width() - modalBox.outerWidth()) / 2
	});
	$([modalBox[0],layer[0]]).fadeOut(300);
}


$(function(){
	var modalForm = $('#modalform');

	$(window).resize(function () {
		modalForm.css({
        	top: ($(window).height() - modalForm.outerHeight()) / 2,
			left: ($(window).width() - modalForm.outerWidth()) / 2
    	});
    });
	
});

function clickOpenFormOpen(){
	modalForm = $('#modalform'),
    // ↑ モーダルウィンドウで表示する要素のIDを指定
    layer = $('<div id=\"div_modalform\" style=\"z-index:10;\" />').appendTo('body').addClass('layer');
		
	$('#modalform').css('display', 'block');
	
	//var iframeSrc = '/sp/form_overtime/';
	//$('#iframe_form_overtime').attr('src',iframeSrc);

	modalForm.css({
	//	top: ($(window).height() - modalForm.outerHeight()) / 2,
		left: ($(window).width() - modalForm.outerWidth()) / 2
	});
	$([modalForm[0],layer[0]]).fadeIn(300);
}

function clickOpenFormClose(){
    modalForm = $('#modalform'),
    // ↑ モーダルウィンドウで表示する要素のIDを指定

	modalForm.css({
		top: ($(window).height() - modalForm.outerHeight()) / 2,
		left: ($(window).width() - modalForm.outerWidth()) / 2
	});
	$([modalForm[0],layer[0]]).fadeOut(300);
}

function clickOpenGmapModalWindow(){
	modalForm = $('#gmap_modal_window'),
    // ↑ モーダルウィンドウで表示する要素のIDを指定
    layer = $('<div id=\"div_modalform\" style=\"z-index:10;\" />').appendTo('body').addClass('layer');
		
	$('#gmap_modal_window').css('display', 'block');
	
	//var iframeSrc = '/sp/form_overtime/';
	//$('#iframe_form_overtime').attr('src',iframeSrc);

	modalForm.css({
	//	top: ($(window).height() - modalForm.outerHeight()) / 2,
		left: ($(window).width() - modalForm.outerWidth()) / 2
	});
	$([modalForm[0],layer[0]]).fadeIn(300);
}

function clickCloseGmapModalWindow(){
    modalForm = $('#gmap_modal_window'),
    // ↑ モーダルウィンドウで表示する要素のIDを指定

	modalForm.css({
		top: ($(window).height() - modalForm.outerHeight()) / 2,
		left: ($(window).width() - modalForm.outerWidth()) / 2
	});
	$([modalForm[0],layer[0]]).fadeOut(300);
}
