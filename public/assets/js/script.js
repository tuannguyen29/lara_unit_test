// JavaScript Document

$(function(){
	$("#fix-content").hide();
	$(window).scroll(function(){
		if($(window).scrollTop() < 100||$(window).scrollTop() > $("#nav").position().top-600) {
			$('#fix-content').slideUp("fast");
		}else{
			$('#fix-content').slideDown("fast");
		}
	});
});

$(function() {

// スムーススクロール
	$('a[href^=#]').not('a[href^=#Tab]').click(function(){
			var speed = 500;
			var href= $(this).attr('href');
			var target = $(href == '#' || href == '' ? 'html' : href);
			var position = target.offset().top;
			$('html, body').animate({scrollTop:position}, speed, 'swing');
			return false;
		});

// アコーディオン
	$('.menu').click(function(){
		var display = $('#nav_menu').css('display');
		if(display == "none"){
			$(this).children().css({'background':'url(/sp/common_img/icon_close.png) no-repeat right center', 'background-size':'15px auto'});
		}else{
			$(this).children().css({'background':'url(/sp/common_img/icon_menu.png) no-repeat right center', 'background-size':'15px auto'});
		}
		$('#nav_menu').slideToggle();
	});


	$('.notice .title, .notice02 .title, .narrowing .title').click(function(){
		var display = $(this).next().css('display');
		if(display == "none"){
			$(this).css({'background':'url(/sp/img/icon_arrow_down_close.png) no-repeat right center', 'background-size':'39px auto'});
		}else{
			$(this).css({'background':'url(/sp/img/icon_arrow_down.png) no-repeat right center', 'background-size':'39px auto'});
		}
		$(this).next().slideToggle();
	});
	$('.list-wrap .title_sub').click(function(){
		var display = $(this).next().css('display');
		if(display == "none"){
			$(this).children().css({'background':'url(/sp/img/icon_arrow_down_close.png) no-repeat right center', 'background-size':'39px auto'});
		}else{
			$(this).children().css({'background':'url(/sp/img/icon_arrow_down.png) no-repeat right center', 'background-size':'39px auto'});
		}
		$(this).next().slideToggle();
	});
	$('.accordion_01').click(function() {
		$('.accordion_02_target').css('display','none')
	    $('.accordion_01_target').slideToggle();
	});
	$('.accordion_02').click(function() {
		$('.accordion_01_target').css('display','none')
	    $('.accordion_02_target').slideToggle();
	});
	
	//2015/05/20 add Dandelion
	/*$('.toritu-menu .public-ttl, .narrowing .title, .next-toritu').click(function(){*/
	$('.toritu-menu .public-ttl, .next-toritu').click(function(){
		$(this).toggleClass("open");
		$(this).next().slideToggle();
	});	

});

$(document).ready(function() {

// カルーセル設定
	$('.carousel').owlCarousel({
		autoPlay: 3000, //Set AutoPlay to 3 seconds
		itemsCustom : [[0, 2]],
		navigation : true,
		responsive : true
	});

});

$(window).resize(function(){

// ウィンドウサイズが変更されたら1秒後にheightLine.js実行
	setTimeout(function(){
        changeBoxSize();
    },1000);
});

// オンマウス画像置換
function imageReplace(){
	var btnClassName = "btn";
	var objAll = document.getElementsByTagName ? document.getElementsByTagName("*") : document.all;
	for (var i = 0; i < objAll.length; i++) {
		var btn=false;
		var eltClass = objAll[i].className.split(/\s+/);
		for (var j = 0; j < eltClass.length; j++) {
			if (eltClass[j] == btnClassName)btn = true;
		}
		if(btn == true){
			if(objAll[i].originalSrc){
				objAll[i].rolloverSrc = objAll[i].originalSrc.replace(/(\.gif|\.jpg|\.png)$/,"_on$1");
					objAll[i].onmouseover = function(){
						this.style.filter ="progid:DXImageTransform.Microsoft.AlphaImageLoader(src="+this.rolloverSrc+",sizingMethod='scale')";
					}
					objAll[i].onmouseout = function(){
						this.style.filter ="progid:DXImageTransform.Microsoft.AlphaImageLoader(src="+this.originalSrc+",sizingMethod='scale')";
					}
			}else{
				if(objAll[i].nodeName=="IMG"){
					objAll[i].originalSrc = objAll[i].src
					objAll[i].rolloverSrc = objAll[i].src.replace(/(\.gif|\.jpg|\.png)$/,"_on$1")
					objAll[i].onmouseover = function(){
						this.src = this.rolloverSrc;
					}
					objAll[i].onmouseout = function(){
						this.src = this.originalSrc;
					}
				}
			}
		}
	}
}
function getElementsByClassName(className){
	var i, j, eltClass;
	var objAll = document.getElementsByTagName ? document.getElementsByTagName("*") : document.all;
	var objCN = new Array();
	for (i = 0; i < objAll.length; i++) {
		eltClass = objAll[i].className.split(/\s+/);
		for (j = 0; j < eltClass.length; j++) {
			if (eltClass[j] == className) {
				objCN.push(objAll[i]);
				break;
			}
		}
	}
	return objCN;
}
function addEvent(elm,listener,fn){
	try{
		elm.addEventListener(listener,fn,false);
	}catch(e){
		elm.attachEvent("on"+listener,fn);
	}
}
addEvent(window,"load",imageReplace)

//アコーディオン（171128）
//　MOOM_L1_-409にて実装されていたが、修正して利用するため form_stopmail/common.jsに移動
// $(function() {
//   //1枚目のパネルを除いて非表示にする
//   $('.ac > dd:gt(0)').hide();
//   $('.ac > dt')
//     .click(function(e){
//       //すべてのパネルを閉じる。
//       $('.ac > dd').slideUp(500);
//       //選択したパネルのみ表示する
//       $('+dd', this).slideDown(500);
//     })
// });
