// JavaScript Document

//必須入力数用
var cemetery_cnt = 1;
var visit_date_cnt = 0;
var visit_date_doji_cnt = 0;
var last_name_cnt = 1;
var first_name_cnt = 1;
var last_kana_cnt = 1;
var first_kana_cnt = 1;
var state_cnt = 1;
var address1_cnt = 1;
var address2_cnt = 1;
var tel_cnt = 1;
var email_cnt = 1;
var konryu_cnt = 1;
var umu_cnt = 1;
var relation_cnt = 1;
var seek_trigger_cnt = 1;

//-------------------------------------------------------------
// 必須入力後に固定フッター表示
//-------------------------------------------------------------
function DispCntUpdate(){
	/*
	if(last_name_cnt==1||first_name_cnt==1||last_kana_cnt==1||first_kana_cnt==1||tel_cnt==1||email_cnt==1||cemetery_cnt==1||visit_date_cnt==1||visit_date_doji_cnt==1||state_cnt==1||address1_cnt==1||address2_cnt==1||konryu_cnt==1||umu_cnt==1||relation_cnt==1||seek_trigger_cnt==1){
		*/
	
	if(last_name_cnt==1||first_name_cnt==1||last_kana_cnt==1||first_kana_cnt==1||tel_cnt==1||email_cnt==1||cemetery_cnt==1||visit_date_cnt==1||visit_date_doji_cnt==1||state_cnt==1||address1_cnt==1||address2_cnt==1||seek_trigger_cnt==1){
		
		//資料請求するボタンの非表示
		$('#fix-btn').css("display", "none");
	
	}else{
		//資料請求するボタンの表示
		$('#fix-btn').css("display", "block");
	}

}

/*********************************************************
 汎用確認メッセージ
*********************************************************/
function ConfirmMsg(msg){
	var result;
	result = (confirm(msg))?true:false;
	return result;
}

function numericZentohan(data)
{
    data += '';
    var table = {
        "０":0,
        "１":1,
        "２":2,
        "３":3,
        "４":4,
        "５":5,
        "６":6,
        "７":7,
        "８":8,
        "９":9
    };

    while(data.match(/[０-９]/)){
        for(n in table){
            data = data.replace(n, table[n]);
        }
    }

    return data;
}

/////////////////////////////////////////////////////////////////////////////////
// 未入力及び不正入力のチェック（入力内容を確認ボタンクリック時）
/////////////////////////////////////////////////////////////////////////////////
function inputChk(confirm_flg){
	
	// フラグの初期化
	var error_mes = "未入力項目があります。\r\n";
	var flg = false;
	var flg_visit_date = false,flg_visit_date_bottom = false;
	$('#error_message').css("display", "none");
		
	// 未入力と不正入力のチェック
	
	inputChk_last_name("1");
	if($("#error_mes_last_name").html()!=""){
		error_mes += $("#error_mes_last_name").html();
		//$("#div_last_name").css("background-color", "#fffee4");
		//$("#div_first_name").css("background-color", "#fffee4");
		flg = true;
	}

	inputChk_first_name("1");
	if($("#error_mes_first_name").html()!=""){
		error_mes += $("#error_mes_first_name").html();
		//$("#div_last_name").css("background-color", "#fffee4");
		//$("#div_first_name").css("background-color", "#fffee4");
		flg = true;
	}

	inputChk_last_kana("1");
	if($("#error_mes_last_kana").html()!=""){
		error_mes += $("#error_mes_last_kana").html();
		//$("#div_last_kana").css("background-color", "#fffee4");
		//$("#div_first_kana").css("background-color", "#fffee4");
		flg = true;
	}

	inputChk_first_kana("1");
	if($("#error_mes_first_kana").html()!=""){
		error_mes += $("#error_mes_first_kana").html();
		//$("#div_last_kana").css("background-color", "#fffee4");
		//$("#div_first_kana").css("background-color", "#fffee4");
		flg = true;
	}

	$("#div_zip").css("background-color", "");

	inputChk_zip("1");
	if($("#error_mes_zip").html()!=""){
		error_mes += $("#error_mes_zip").html();
		//$("#div_zip").css("background-color", "#fffee4");
		flg = true;
	}
	
	$("#div_state").css("background-color", "");

	inputChk_state("1");
	if($("#error_mes_state").html()!=""){
		error_mes += $("#error_mes_state").html();
		//$("#div_state").css("background-color", "#fffee4");
		flg = true;
	}
	
	$("#div_address1").css("background-color", "");
	
	inputChk_address1("1");
	if($("#error_mes_address1").html()!=""){
		error_mes += $("#error_mes_address1").html();
		//$("#div_address1").css("background-color", "#fffee4");
		flg = true;
	}
	
	$("#div_address2").css("background-color", "");

	inputChk_address2("1");
	if($("#error_mes_address2").html()!=""){
		error_mes += $("#error_mes_address2").html();
		//$("#div_address2").css("background-color", "#fffee4");
		flg = true;
	}

	$("#div_tel").css("background-color", "");

	inputChk_tel("1");
	if($("#error_mes_tel").html()!=""){
		error_mes += $("#error_mes_tel").html();
		//$("#div_tel").css("background-color", "#fffee4");
		flg = true;
	}

	$("#div_email").css("background-color", "");

	inputChk_email("1");
	if($("#error_mes_email").html()!=""){
		error_mes += $("#error_mes_email").html();
		//$("#div_email").css("background-color", "#fffee4");
		flg = true;
	}

	$("#div_seek_trigger").css("background-color", "");
	
	inputChk_seek_trigger("1");
	if($("#error_mes_seek_trigger").html()!=""){
		error_mes += $("#error_mes_seek_trigger").html();
		//$("#div_seek_trigger").css("background-color", "#fffee4");
		flg = true;
	}
	
	//inputChk_cart("1");
	//if($("#error_mes_cart").html()!=""){
	//	$(".list-table").css("background-color", "#fffee4");
	//	flg = true;
	//}
	
	if(inputChk_visit("1")){
		error_mes += "見学日時を入力してください。\r\n";
		flg_visit_date = true;
		flg = true;
	}
	
	// 判定
	if(flg){
			
		window.alert(error_mes);

		//$('#error_message').css("display", "block");

		// アラート表示して再入力を警告
		//window.alert($('#err_name').html());
		//$('#err_message').html("恐れ入りますが、上記の内容をご確認ください");
		var location_flg = false;

		if($("#error_mes_last_name").html()!=""){
			if(location_flg == false){
				var divOffset = $('#div_last_name').offset().top;
				$('html,body').animate({scrollTop: divOffset + 'px'}, 50);

				location_flg = true;
			}
		}

		if($("#error_mes_last_kana").html()!=""){
			if(location_flg == false){
				var divOffset = $('#div_last_kana').offset().top;
				$('html,body').animate({scrollTop: divOffset + 'px'}, 50);

				location_flg = true;
			}
		}

		if($("#error_mes_email").html()!=""){
			if(location_flg == false){
				var divOffset = $('#div_email').offset().top;
				$('html,body').animate({scrollTop: divOffset + 'px'}, 50);

				location_flg = true;
			}
		}
		
		if($("#error_mes_tel").html()!=""){
			if(location_flg == false){
				var divOffset = $('#div_tel').offset().top;
				$('html,body').animate({scrollTop: divOffset + 'px'}, 50);

				location_flg = true;
			}
		}

		if($('#error_mes_zip').html()!="" || $('#error_mes_state').html()!="" || $('#error_mes_address1').html()!="" || $('#error_mes_address2').html()!=""){
			if(location_flg == false){
				var divOffset = $('#div_zip').offset().top;
				$('html,body').animate({scrollTop: divOffset + 'px'}, 50);

				location_flg = true;
			}
		}

		if($("#error_mes_seek_trigger").html()!=""){
			if(location_flg == false){
				var divOffset = $('#div_seek_trigger').offset().top;
				$('html,body').animate({scrollTop: divOffset + 'px'}, 50);

				location_flg = true;
			}
		}

		if(flg_visit_date==true){
			if(location_flg == false){
				var divOffset = $('.list-table').offset().top;
				$('html,body').animate({scrollTop: divOffset + 'px'}, 50);

				location_flg = true;
			}
		}

		if($("#error_mes_cart").html()!=""){
			if(location_flg == false){
				var divOffset = $('#error_mes_cart').offset().top;
				$('html,body').animate({scrollTop: divOffset + 'px'}, 50);

				location_flg = true;
			}
		}

		return false;
	}
	else{
		
		// 確認メッセージ
		if(confirm_flg){
			return ConfirmMsg('ご入力いただいた内容で送信します。\nよろしいですか？');
		}
		return true;
	}


}

//-------------------------------------------------------------
// 「カート」のチェック
//-------------------------------------------------------------
function inputChk_cart(dsp_msg){

	var data = new Object();
	data.site_id = $('#site_id').val();
	
	$.ajax({
		url:'/regist/LGC_CheckCemetery.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {
			
			results = res_json.split(",");
			
			if(decodeURIComponent(results[1])=="1"){
				$('#error_mes_cart').html(decodeURIComponent(results[0]));
				$('#error_mes_cart').css("display", "block");
				$(".list-table").css("background-color", "#fffee4");
				cemetery_cnt = 1;
			}else{
				$('#error_mes_cart').html("");
				$(".list-table").css("background-color", "");
				$('#error_mes_cart').css("display", "none");
				cemetery_cnt = 0;
			}

		},
		error:function(res, status, e) {
		}
	});
	
	DispCntUpdate();
}

//-------------------------------------------------------------
// 「お名前（姓）」の入力チェック
//-------------------------------------------------------------
function inputChk_last_name(dsp_msg){

	if($("#last_name").val()==""){
		if(dsp_msg=="1"){
			$("#error_mes_last_name").html("姓を入力してください。\r\n");

			//$("#last_name").css("border", "1px solid #bf0721");
			$("#last_name").css("background-color", "#fffee4");
		}else{
			//$("#error_mes_last_name").html("");
			//$("#last_name").css("border", "1px solid #fffee4");
			$("#last_name").css("background-color", "#fffee4");
		}
		
		last_name_cnt = 1;
	}else{
		$("#error_mes_last_name").html("");
		//$("#last_name").css("border", "");
		$("#last_name").css("background-color", "");
		
		last_name_cnt = 0;
	}
	
	DispCntUpdate();

}

//-------------------------------------------------------------
// 「お名前（名）」の入力チェック
//-------------------------------------------------------------
function inputChk_first_name(dsp_msg){

	if($("#first_name").val()==""){
		if(dsp_msg=="1"){
			$("#error_mes_first_name").html("名を入力してください。\r\n");

			//$("#first_name").css("border", "1px solid #bf0721");
			$("#first_name").css("background-color", "#fffee4");
		}else{
			//$("#error_mes_first_name").html("");
			//$("#first_name").css("border", "1px solid #fffee4");
			$("#first_name").css("background-color", "#fffee4");
		}
		
		first_name_cnt = 1;
	}else{
		$("#error_mes_first_name").html("");
		//$("#first_name").css("border", "");
		$("#first_name").css("background-color", "");
		
		first_name_cnt = 0;
	}
	
	DispCntUpdate();

}

//-------------------------------------------------------------
// 「ふりがな（せい）」の入力チェック
//-------------------------------------------------------------
function inputChk_last_kana(dsp_msg){

	if($("#last_kana").val()==""){
		if(dsp_msg=="1"){
			$("#error_mes_last_kana").html("せいを入力してください。\r\n");

			//$("#last_kana").css("border", "1px solid #bf0721");
			$("#last_kana").css("background-color", "#fffee4");
		}else{
			//$("#error_mes_last_kana").html("");
			//$("#last_kana").css("border", "1px solid #fffee4");
			$("#last_kana").css("background-color", "#fffee4");
		}
		
		last_kana_cnt = 1;
	}else{
		$("#error_mes_last_kana").html("");
		//$("#last_kana").css("border", "");
		$("#last_kana").css("background-color", "");
		
		last_kana_cnt = 0;
	}
	
	DispCntUpdate();

}

//-------------------------------------------------------------
// 「ふりがな（めい）」の入力チェック
//-------------------------------------------------------------
function inputChk_first_kana(dsp_msg){

	if($("#first_kana").val()==""){
		if(dsp_msg=="1"){
			$("#error_mes_first_kana").html("めいを入力してください。\r\n");

			//$("#first_kana").css("border", "1px solid #bf0721");
			$("#first_kana").css("background-color", "#fffee4");
		}else{
			//$("#error_mes_first_kana").html("");
			//$("#first_kana").css("border", "1px solid #fffee4");
			$("#first_kana").css("background-color", "#fffee4");
		}
		
		first_kana_cnt = 1;
	}else{
		$("#error_mes_first_kana").html("");
		//$("#first_kana").css("border", "");
		$("#first_kana").css("background-color", "");
		
		first_kana_cnt = 0;
	}
	
	DispCntUpdate();

}

//-------------------------------------------------------------
// 「メールアドレス」の入力チェック
//-------------------------------------------------------------
function inputChk_email(dsp_msg){

	if($("#email").val()==""){
		if(dsp_msg=="1"){
			$("#error_mes_email").html("メールアドレスを入力してください。\r\n");
			//$("#email").css("border", "1px solid #bf0721");
			$("#email").css("background-color", "#fffee4");
		}else{
			//$("#error_mes_email").html("");
			//$("#email").css("border", "1px solid #fffee4");
			$("#email").css("background-color", "#fffee4");
		}
		
		email_cnt = 1;
	}else{
		var mail_regex1 = new RegExp( '(?:[-!#-\'*+/-9=?A-Z^-~]+\.?(?:\.[-!#-\'*+/-9=?A-Z^-~]+)*|"(?:[!#-\[\]-~]|\\\\[\x09 -~])*")@[-!#-\'*+/-9=?A-Z^-~]+(?:\.[-!#-\'*+/-9=?A-Z^-~]+)*' );
		var mail_regex2 = new RegExp( '^[^\@]+\@[^\@]+$' );
		if( $("#email").val().match( mail_regex1 ) && $("#email").val().match( mail_regex2 ) ) {
			// 全角チェック 及び　末尾TLDチェック（〜.co,jpなどの末尾ミスチェック用）
			if( $("#email").val().match( /[^a-zA-Z0-9\!\"\#\$\%\&\'\(\)\=\~\|\-\^\\\@\[\;\:\]\,\.\/\\\<\>\?\_\`\{\+\*\} ]/ ) || ( !$("#email").val().match( /\.[a-z]+$/ ) ) ) {
				if(dsp_msg=="1"){
					$("#error_mes_email").html("メールアドレスの形式に誤りがあります。\r\n");
				}else{
					$("#error_mes_email").html("");
				}
				//$("#email").css("border", "1px solid #bf0721");
				$("#email").css("background-color", "#fffee4");
				
				email_cnt = 1;
			}else{
				$("#error_mes_email").html("");
				//$("#email").css("border", "1px solid #fffee4");
				//$("#email").css("background-color", "#fffee4");
				
				email_cnt = 0;
			}
		} else {
			if(dsp_msg=="1"){
				$("#error_mes_email").html("メールアドレスの形式に誤りがあります。\r\n");
			}else{
				$("#error_mes_email").html("");
			}
			//$("#email").css("border", "1px solid #bf0721");
			$("#email").css("background-color", "#fffee4");
			
			email_cnt = 1;
		}

	}
	
	DispCntUpdate();

}

//-------------------------------------------------------------
// 「電話番号」の入力チェック
//-------------------------------------------------------------
function inputChk_tel(dsp_msg){

	if(($("#tel").val()!="" && (numericZentohan($("#tel").val()).match(/^[-0-9]{6,14}$/)==null))){
		
		if(dsp_msg=="1"){
			$("#error_mes_tel").html("電話番号を正しく入力してください。\r\n");
			//$("#tel").css("border", "1px solid #bf0721");
			$("#tel").css("background-color", "#eecbd0");
		}else{
			//$("#error_mes_tel").html("");
			//$("#tel").css("border", "1px solid #fffee4");
			$("#tel").css("background-color", "#fffee4");
		}
		
		tel_cnt = 1;
	}else{
		if($("#tel").val()==""){
			if(dsp_msg=="1"){
				$("#error_mes_tel").html("電話番号を入力してください。\r\n");
				//$("#tel").css("border", "1px solid #bf0721");
				$("#tel").css("background-color", "#fffee4");
			}else{
				$("#error_mes_tel").html("");
				//$("#tel").css("border", "1px solid #fffee4");
				$("#tel").css("background-color", "#fffee4");
			}
			
			tel_cnt = 1;
		}else{
			$("#error_mes_tel").html("");
			//$("#tel").css("border", "");
			$("#tel").css("background-color", "");
			
			tel_cnt = 0;
		}
	}
	
	DispCntUpdate();

}

//-------------------------------------------------------------
// 「郵便番号」の入力チェック
//-------------------------------------------------------------
function inputChk_zip(dsp_msg){

	if($("#zip").val()==""){
	/*
		if(dsp_msg=="1"){
			$("#error_mes_zip").html("郵便番号を入力してください。\r\n");
		}else{
			//$("#error_mes_zip").html("");
		}
		$("#zip").css("border", "1px solid #bf0721");
		$("#zip").css("background-color", "#eecbd0");
		*/

		$("#error_mes_zip").html("");
		//$("#zip").css("border", "");
		$("#zip").css("background-color", "#fff");
	}else{
		if(isNaN(numericZentohan($("#zip").val()))){
			if(dsp_msg=="1"){
				//$("#error_mes_zip").html("郵便番号は数字のみでご記入ください。\r\n");
				$("#error_mes_zip").html("郵便番号を正しく入力してください。\r\n");
			}else{
				$("#error_mes_zip").html("");
			}
			//$("#zip").css("border", "1px solid #bf0721");
			$("#zip").css("background-color", "#fffee4");
		}else{
			$("#error_mes_zip").html("");
			//$("#zip").css("border", "");
			$("#zip").css("background-color", "#fff");
		}
	}
	
	DispCntUpdate();

}

//-------------------------------------------------------------
// 「都道府県」の入力チェック
//-------------------------------------------------------------
function inputChk_state(dsp_msg){

	if($("#state").val()==""){
		if(dsp_msg=="1"){
			$("#error_mes_state").html("都道府県を選択してください。\r\n");

			//$("#state").css("border", "1px solid #bf0721");
			$("#state").css("background-color", "#fffee4");
		}else{
			//$("#error_mes_state").html("");
			//$("#state").css("border", "1px solid #fffee4");
			$("#state").css("background-color", "#fffee4");
		}
		
		state_cnt = 1;
	}else{
		$("#error_mes_state").html("");
		//$("#state").css("border", "");
		$("#state").css("background-color", "");
		
		state_cnt = 0;
	}
	
	DispCntUpdate();

}

//-------------------------------------------------------------
// 「「住所1」の入力チェック
//-------------------------------------------------------------
function inputChk_address1(dsp_msg){

	if($("#address1").val()==""){
		if(dsp_msg=="1"){
			$("#error_mes_address1").html("市区町村名を入力してください。\r\n");
			//$("#address1").css("border", "1px solid #bf0721");
			$("#address1").css("background-color", "#fffee4");
		}else{
			//$("#error_mes_address1").html("");
			//$("#address1").css("border", "1px solid #fffee4");
			$("#address1").css("background-color", "#fffee4");
		}
		
		address1_cnt = 1;
	}else{
		$("#error_mes_address1").html("");
		//$("#address1").css("border", "");
		$("#address1").css("background-color", "");
		
		address1_cnt = 0;
	}
	
	DispCntUpdate();

}

//-------------------------------------------------------------
// 「「住所2」の入力チェック
//-------------------------------------------------------------
function inputChk_address2(dsp_msg){

	if($("#address2").val()==""){
		if(dsp_msg=="1"){
			$("#error_mes_address2").html("町名・番地・建物名を入力してください。\r\n");
			//$("#address2").css("border", "1px solid #bf0721");
			$("#address2").css("background-color", "#fffee4");
		}else{
			//$("#error_mes_address1").html("");
			//$("#address2").css("border", "1px solid #fffee4");
			$("#address2").css("background-color", "#fffee4");
		}
		
		address2_cnt = 1;
	}else{
		$("#error_mes_address2").html("");
		//$("#address2").css("border", "");
		$("#address2").css("background-color", "");
		
		address2_cnt = 0;
	}
	
	DispCntUpdate();

}

//-------------------------------------------------------------
// 「今回お墓探しをしようと思ったきっかけ」の入力チェック
//-------------------------------------------------------------
function inputChk_seek_trigger(dsp_msg){

	if($("#seek_trigger").val()==""){
		if(dsp_msg=="1"){
			$("#error_mes_seek_trigger").html("お墓探しをしようと思ったきっかけを選択してください。\r\n");
			//$("#seek_trigger").css("border", "1px solid #bf0721");
			$("#seek_trigger").css("background-color", "#fffee4");
		}else{
			//$("#error_mes_email").html("");
			//$("#seek_trigger").css("border", "1px solid #fffee4");
			$("#seek_trigger").css("background-color", "#fffee4");
		}
		
		seek_trigger_cnt = 1;
	}else{
		if($("#seek_trigger").val()=="その他"&&$("#seek_trigger_other").val()==""){

			//$("#error_mes_seek_trigger").html("その他を選択した場合は、どのようなきっかけなのかをご記入ください。\r\n");
			//$("#seek_trigger_other").css("border", "1px solid #bf0721");
			//$("#seek_trigger_other").css("background-color", "#eecbd0");

			seek_trigger_cnt = 1;
		}else{
		
			$("#error_mes_seek_trigger").html("");
			//$("#seek_trigger_other").css("border", "");
			$("#seek_trigger_other").css("background-color", "");
			//$("#seek_trigger").css("border", "");
			$("#seek_trigger").css("background-color", "");
			
			seek_trigger_cnt = 0;
		
		}

	}
	
	DispCntUpdate();

}

//-------------------------------------------------------------
// 「見学日時」の入力チェック（上一覧）
//-------------------------------------------------------------
function inputChk_visit(dsp_msg){
	
	var visit_flg = false;
	
	$('[name="visit[]"]:checked').each(function(){
		if(inputChk_vist_date("1",$(this).val())){
			$("#td_visit_date_"+$(this).val()+"_l").css("background-color", "");
			$("#td_visit_date_"+$(this).val()+"_r").css("background-color", "");
		}else{
			$("#td_visit_date_"+$(this).val()+"_l").css("background-color", "#fffee4");
			$("#td_visit_date_"+$(this).val()+"_r").css("background-color", "#fffee4");
			visit_flg = true;
		}
		
	});
	
	if(visit_flg){
		visit_date_cnt = 1;
		DispCntUpdate();

		return true;
	}else{
		visit_date_cnt = 0;
		DispCntUpdate();

		return false;
	}

}

//-------------------------------------------------------------
// 「見学日時」の入力チェック（上一覧）
//-------------------------------------------------------------
function inputChk_vist_date(dsp_msg,visit_id){
	
	if($("#visit"+visit_id).prop('checked')==true && $("#visit_date_"+visit_id).val()==''){
		if(dsp_msg=="1"){
			$("#error_mes_visit_date_"+visit_id).html("見学日時を入力してください。\r\n");

			//$("#visit_date_"+visit_id).css("border", "1px solid #bf0721");
			$("#visit_date_"+visit_id).css("background-color", "#eecbd0");
				
			return false;
		}else{
			//$("#error_mes_email").html("");
			//$("#visit_date_"+visit_id).css("border", "1px solid #fffee4");
			$("#visit_date_"+visit_id).css("background-color", "#fffee4");
				
			return true;
		}
	}else{
		//$("#visit_date_"+visit_id).css("border", "");
		$("#visit_date_"+visit_id).css("background-color", "");
		$("#error_mes_visit_date_"+visit_id).html("");
				
		return true;
	}

}

//-------------------------------------------------------------
// 「見学コメント」表示のチェック
//-------------------------------------------------------------
function disp_visit_message(){

	var vist_check=[];
	$('[name="visit[]"]:checked').each(function(){
		vist_check.push($(this).val());
	});

	if(vist_check.length > 0){
		$("#visit_message").css("display", "");
		$("#visit_message2").css("display", "");
	}else{
		$("#visit_message").css("display", "none");
		$("#visit_message2").css("display", "none");
	}

}


//-------------------------------------------------------------
//  「資料請求」チェック関連チェック
//-------------------------------------------------------------
function siryo_check(item_id,siryo_check_id,visit_check_id){
	
	if($("#"+siryo_check_id).prop('checked')) {
		//資料請求のチェックボックスがチェックされている場合
	}else{
		//資料請求のチェックボックスがチェックされていない場合
		if($("#"+visit_check_id).prop('checked')) {
			//見学希望のチェックボックスがチェックされている場合
			$('#'+visit_check_id).prop("checked",false);
			updateCartVisit(item_id);
			$('#tr_visit_date_'+item_id).hide();
			//disp_visit_remark();
			disp_visit_message();
		}
	}

}

//-------------------------------------------------------------
//  「見学希望」チェック関連チェック
//-------------------------------------------------------------
function visit_check(item_id,siryo_check_id,visit_check_id){

	if($("#"+visit_check_id).prop('checked')) {
		//見学希望のチェックボックスがチェックされている場合
		if($("#"+siryo_check_id).prop('checked')!=true) {
			$('#'+siryo_check_id).prop("checked",true);
			updateCart(item_id);
		}
	}else{
		//見学希望のチェックボックスがチェックされていない場合
	}

}
