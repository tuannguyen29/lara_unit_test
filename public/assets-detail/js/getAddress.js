//郵便番号から住所情報取得
function updateAddress(zip1,zip2,state,address1) {
  
  // Ajax通信
	var data = new Object();

	data.zip1 = $('#'+zip1).val();
	data.zip2 = $('#'+zip2).val();
	
	$.ajax({
		url:'/common_lib/LGC_AddressSet.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {
			
			results = res_json.split(",");
	
			if(state==""){
				$('#'+address1).val(decodeURIComponent(results[0])+decodeURIComponent(results[1]));
			}else{
				$('#'+state).val(decodeURIComponent(results[0]));
				$('#'+address1).val(decodeURIComponent(results[1]));
			}

		},
		error:function(res, status, e) {
		}
	});

} 
