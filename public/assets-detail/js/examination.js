//検討中リスト
//カート操作プラス、マイナス
function updateExamination(product_id,add_only,del_only,del_all) {
	
	var data = new Object();
	
	data.pid = product_id;
	data.add_only = add_only;
	data.del_only = del_only;
	data.del_all = del_all;

	$.ajax({
		url:'/common_lib/getExaminationList.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {

			results = res_json.split(",");
			
			//window.alert(decodeURIComponent(results[2]));
			
			if(del_all=="1"){
				//資料請求リストを空にするを行った場合
				
				//削除したリストから存在する要素の表示を切り替える
				var array_del_list = new Array();
				array_del_list = decodeURIComponent(results[5]).split("|");
				
				//window.alert(decodeURIComponent(results[5]));
				
				for (var i=0 ; i<array_del_list.length ; i++){
  					if(array_del_list[i] != ""){
						//window.alert(array_del_list[i]);
	
						//チェックボックスが存在する場合は設定（PC一覧ページ）（ピックアップ）
						if(0 < $("#pickup_cart_check_area"+array_del_list[i]).size() && (0 >= $("#cart_cnt").size())){
							//存在する
							$("#pickup_cart_check_area"+array_del_list[i]).css('background','#EAEAEA');
							$("#pickup_cart_check_area"+array_del_list[i]).children('input').prop({'checked':false});
							$("#pickup_cart_check_area"+array_del_list[i]).children('img').attr("src","/search/img/img_checkbox2_off.png");
						}else{
							//存在しない
						}
	
						//チェックボックスが存在する場合は設定（PC一覧ページ）
						if(0 < $("#cart_check_area"+array_del_list[i]).size() && (0 >= $("#cart_cnt").size())){
							//存在する
							$("#cart_check_area"+array_del_list[i]).css('background','#EAEAEA');
							$("#cart_check_area"+array_del_list[i]).children('input').prop({'checked':false});
							$("#cart_check_area"+array_del_list[i]).children('img').attr("src","/search/img/img_checkbox2_off.png");
						}else{
							//存在しない
						}
	
						//検討中リストのボタンが存在する場合は設定（詳細ページ）
						if(0 < $("#examination_btn"+array_del_list[i]).size()){
							//存在する
							$("#examination_btn"+array_del_list[i]).attr("src","/search/img/btn-list.png");
						}else{
							//存在しない
						}
	
						//検討中リストのテキストが存在する場合は設定（詳細ページ）
						if(0 < $("#detail_ex_text").size()){
							//存在する
							$("#detail_ex_text").html("検討リストに追加する");
						}else{
							//存在しない
						}
		
						//検討中リストのボタンが存在する場合は設定（詳細ページ）下
						if(0 < $("#examination_btn-bottom-"+array_del_list[i]).size()){
							//存在する
							$("#examination_btn-bottom-"+array_del_list[i]).attr("src","/search/img/btn-list.png");
						}else{
							//存在しない
						}
		
						//この霊園を選んだ方は以下の霊園・墓地も見ています のボタンを変更
						if(0 < $("#seikyu_btn"+array_del_list[i]).size()){
							//存在する
							$("#seikyu_btn"+array_del_list[i]).attr("src","/detail/img/btn-plus.gif");
						}else{
							//存在しない
						}
						
						//現在地周辺(10km圏内)の霊園・墓地 のGoogleバルーン内を変更
						if(0 < $('#iframe'+array_del_list[i]).contents().find("#gex_link_"+array_del_list[i]).size()){
							//存在する
							$('#iframe'+array_del_list[i]).contents().find("#gex_link_"+array_del_list[i]).html("資料請求に追加");
						}else{
							//存在しない
						}

					}
				}

			}else{		
			
				if(decodeURIComponent(results[1])=="3"){
					//削除した場合 -- 3
	
					//検討中リストのボタンが存在する場合は設定（詳細ページ）
					if(0 < $("#examination_btn"+product_id).size()){
						//存在する
						$("#examination_btn"+product_id).attr("src","/search/img/btn-list.png");
					}else{
						//存在しない
					}
	
					//検討中リストのテキストが存在する場合は設定（詳細ページ）
					if(0 < $("#detail_ex_text").size()){
						//存在する
						$("#detail_ex_text").html("検討リストに追加する");
					}else{
						//存在しない
					}
	
					//検討中リストのボタンが存在する場合は設定（詳細ページ）下
					if(0 < $("#examination_btn-bottom-"+product_id).size()){
						//存在する
						$("#examination_btn-bottom-"+product_id).attr("src","/search/img/btn-list.png");
					}else{
						//存在しない
					}
	
					//この霊園を選んだ方は以下の霊園・墓地も見ています のボタンを変更
					if(0 < $("#seikyu_btn"+product_id).size()){
						//存在する
						$("#seikyu_btn"+product_id).attr("src","/detail/img/btn-plus.gif");
					}else{
						//存在しない
					}
	
					//現在地周辺(10km圏内)の霊園・墓地 のGoogleバルーン内を変更
					if(0 < $("#gex_link_"+product_id).size()){
						//存在する
						$("#gex_link_"+product_id).html("資料請求に追加");
					}else{
						//存在しない
					}
					
					//スマホ詳細ページ
					if(0 < $("#examination_detail1"+product_id).size()){
						//存在する
						$("#examination_detail1_li"+product_id).addClass("add");
						$("#examination_detail1_li"+product_id).removeClass("ex_minus");
						$("#examination_detail1"+product_id).html("+ 資料請求に追加");
					}else{
						//存在しない
					}
					
					if(0 < $("#examination_detail2"+product_id).size()){
						//存在する
						$("#examination_detail2_li"+product_id).addClass("add");
						$("#examination_detail2_li"+product_id).removeClass("ex_minus");
						$("#examination_detail2"+product_id).html("+ 資料請求に追加");
					}else{
						//存在しない
					}
					
					//スマホ詳細ページ この霊園を選んだ方は以下の霊園・墓地も見ています
					if(0 < $("#examination_link"+product_id).size()){
						//存在する
						$("#examination_link_li"+product_id).removeClass("minus");
						$("#examination_link_li"+product_id).addClass("plus");
						$("#examination_link"+product_id).html("+ 資料請求に追加");
					}else{
						//存在しない
					}
					
				}else{
					//追加した場合 -- 2
	
					//検討中リストのボタンが存在する場合は設定（詳細ページ）
					if(0 < $("#examination_btn"+product_id).size()){
						//存在する
						$("#examination_btn"+product_id).attr("src","/search/img/btn-list-selected.png");
					}else{
						//存在しない
					}
	
					//検討中リストのテキストが存在する場合は設定（詳細ページ）
					if(0 < $("#detail_ex_text").size()){
						//存在する
						$("#detail_ex_text").html("検討リストに追加済");
					}else{
						//存在しない
					}
	
					//検討中リストのボタンが存在する場合は設定（詳細ページ）下
					if(0 < $("#examination_btn-bottom-"+product_id).size()){
						//存在する
						$("#examination_btn-bottom-"+product_id).attr("src","/search/img/btn-list-selected.png");
					}else{
						//存在しない
					}
	
					//この霊園を選んだ方は以下の霊園・墓地も見ています のボタンを変更
					if(0 < $("#seikyu_btn"+product_id).size()){
						//存在する
						$("#seikyu_btn"+product_id).attr("src","/detail/img/btn-minus.gif");
					}else{
						//存在しない
					}
	
					//現在地周辺(10km圏内)の霊園・墓地 のGoogleバルーン内を変更
					if(0 < $("#gex_link_"+product_id).size()){
						//存在する
						$("#gex_link_"+product_id).html("資料請求に追加済");
					}else{
						//存在しない
					}
					
					//スマホ詳細ページ
					if(0 < $("#examination_detail1"+product_id).size()){
						//存在する
						$("#examination_detail1_li"+product_id).removeClass("add");
						$("#examination_detail1_li"+product_id).addClass("ex_minus");
						$("#examination_detail1"+product_id).html("- 資料請求に追加済");
					}else{
						//存在しない
					}
					
					if(0 < $("#examination_detail2"+product_id).size()){
						//存在する
						$("#examination_detail2_li"+product_id).removeClass("add");
						$("#examination_detail2_li"+product_id).addClass("ex_minus");
						$("#examination_detail2"+product_id).html("- 資料請求に追加済");
					}else{
						//存在しない
					}
					
					//スマホ詳細ページ この霊園を選んだ方は以下の霊園・墓地も見ています
					if(0 < $("#examination_link"+product_id).size()){
						//存在する
						$("#examination_link_li"+product_id).removeClass("plus");
						$("#examination_link_li"+product_id).addClass("minus");
						$("#examination_link"+product_id).html("- 資料請求に追加済");
					}else{
						//存在しない
					}
					
				}
			
			}

			//window.alert(decodeURIComponent(results[2]));
			//検討中リストの表示エリアが存在する場合は設定
			if(0 < $("#examination-list").size()){
				//存在する
				$("#examination-list").html(decodeURIComponent(results[3]));
					
			}else{
				//存在しない
			}
	
			//iframeから親の検討中リストの表示エリアが存在する場合は設定
			if(0 < $("#examination-list",parent.document).size()){
				//存在する
				$("#examination-list",parent.document).html(decodeURIComponent(results[3]));
					
			}else{
				//存在しない
			}
	
			//検討中リスト（スマホ）の表示エリアが存在する場合は設定
			if(0 < $("#examination-list_sp").size()){
				//存在する
				$("#examination-list_sp").html(decodeURIComponent(results[4]));
					
			}else{
				//存在しない
			}

		},
			error:function(res, status, e) {
		}
	});
				
	return false;

}

//検討中リストから資料請求カートに追加
function ExaminationCart() {
	
	var data = new Object();

	$.ajax({
		url:'/common_lib/ExaminationListCart.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {

			results = res_json.split(",");
			
			//window.alert(decodeURIComponent(results[2]));

		},
			error:function(res, status, e) {
		}
	});
				
	//資料請求ページに遷移
	location.href='/regist/';
	
	return false;

}

//検討中リストから資料請求カートに追加（スマホ用）
function ExaminationCart_sp() {
	
	var data = new Object();

	$.ajax({
		url:'/common_lib/ExaminationListCart.php',
		type:'POST',
		data:data,
		dataType:'text',
		async:false,
		success:function(res_json) {

			results = res_json.split(",");
			
			//window.alert(decodeURIComponent(results[2]));

		},
			error:function(res, status, e) {
		}
	});
				
	//資料請求ページに遷移
	location.href='/sp/regist/';
	
	return false;

}

function Examination_close() {
	$(".examination").attr("src","/common_img/header/btn_shiryou.png");
	$(".examination").removeClass("open")
	$("#examination-list").slideToggle("fast");
}
