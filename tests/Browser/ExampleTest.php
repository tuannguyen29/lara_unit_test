<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;

class ExampleTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->browse(function ($browser) {
            $browser->visit('/')
                ->assertSee('Laravel');
        });

        $this->browse(function ($browser) {
            $browser->visit('/')
                    ->waitForText('Dusk', 10)
                    ->assertSee('Dusk');
        });

        // $this->browse(function ($first, $second) {
        //     $first->loginAs(User::find(1))
        //             ->visit('/home')
        //             ->waitForText('Message');

        //     $second->loginAs(User::find(2))
        //             ->visit('/home')
        //             ->waitForText('Message')
        //             ->type('message', 'Hey Taylor')
        //             ->press('Send');

        //     $first->waitForText('Hey Taylor')
        //            ->assertSee('Jeffrey Way');
        // });
    }
}
