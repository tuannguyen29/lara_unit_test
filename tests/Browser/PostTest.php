<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Faker\Factory as Faker;

class PostTest extends DuskTestCase
{
    protected $faker;

    public function setUp()
    {
        parent::setUp();
        $this->faker = Faker::create();
    }
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testViewPost()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/posts')
                    ->assertSee('Create a post')
                    ->click('.add-post')
                    ->waitForLocation('/posts/create')
                    ->assertSee('input enter post');
        });

    }

    // public function testCreatePost()
    // {
    //     $this->browse(function (Browser $browser) {
    //         $browser->visit('/posts/create')
    //                 ->assertSee('input enter post')
    //                 ->type('title', 'nguyentuan')
    //                 ->type('description', 'description nguyentuan')
    //                 ->press('Submit')
    //                 ->waitForLocation('/posts');
    //     });
    // }

    public function testRegister()
    {
        $this->browse(function ($browser) {
        $emailRandom = $this->faker->word;
            $browser->visit('register')
                    ->type('name', 'Tuan Nguyen')
                    ->type('email', $emailRandom.'@mulodo.com')
                    ->type('password', 'minhtuan')
                    ->type('password_confirmation', 'minhtuan')
                    ->press('Register')
                    ->assertPathIs('/home');
        });
    }

    public function testLogout()
    {
        $this->browse(function ($browser) {
            $browser->visit('/home')
                    ->pause(3000)
                    ->clickLink('Logout')
                    ->pause(3000)
                    ->assertPathIs('/')
                    ->pause(4000);
        });
    }

    // public function testLogin()
    // {
    //     $this->browse(function ($browser) {
    //         $browser->visit('login')
    //                 ->type('email', 'tuannguyen@mulodo.com')
    //                 ->type('password', 'minhtuan')
    //                 ->press('Login')
    //                 ->assertPathIs('/home')
    //                 ->pause(4000);
    //     });
    // }
}

