<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Functional extends \Codeception\Module
{
    public function seeTextExist($text) {
        // $this->see($text);
        $this->assertTrue(isset($text), "this thing is set");
    }
}
