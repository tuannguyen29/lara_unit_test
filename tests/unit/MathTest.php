<?php
class MathTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $calculator;

    public function _inject(\Helper\CalculatorHelper $calculator) {
        $this->calculator = $calculator;
    }

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testAdd()
    {
        $expected = 5;
        $actual = $this->calculator->add(2, 3);
        $this->assertEquals($expected, $actual);
    }

     public function testSubstract()
    {
        $expected = 2;
        $actual = $this->calculator->substract(5, 3);
        $this->assertEquals($expected, $actual);
    }

    public function testMultiply()
    {
        $expected = 20;
        $actual = $this->calculator->multiply(10, 2);
        $this->assertEquals($expected, $actual);
    }

    public function testDivide()
    {
        $expected = 15;
        $actual = $this->calculator->divide(45, 3);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testExceptionProducedWhileDividingByZero()
    {
        $this->calculator->divide(45, 0);
    }
}