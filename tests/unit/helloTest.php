<?php

use App\Service\BarTestService;


class helloTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testSomeFeature()
    {
        // $a = "ttttttt";
        // $this->assertEquals('Example WWW Page', $a);

        $mock = \Mockery::mock(BarTestService::class);
        $mock->shouldReceive('printText')->once()->andReturn("tuan");

        $this->assertEquals('tuan', $mock->printText());

    }
}