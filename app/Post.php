<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    protected $table = 'post';
    protected $fillable = ['title', 'description'];
    protected $hidden = [];

    public function getAllPost()
    {
        $posts = DB::table($this->table)->get();
        return $posts;
    }
}
