<?php

namespace App\Http\Controllers\Sp;

use App\Cemetery;
use App\Http\Controllers\Controller;

class TopController extends Controller
{
    private $cemetery_lts;

    public function __construct(Cemetery $cemetery_lts)
    {
        $this->cemetery_lts = $cemetery_lts;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // We can't test it.
        // $listCemeteries = Cemetery::paginate(5);
        $listCemeteries = $this->cemetery_lts->all();
        return view('sp.top.index', ['listCemeteries' => $listCemeteries]);
    }

    public function detail()
    {
        return view('sp.detail.index');
    }

    public function detail_review_list()
    {
        return view('sp.detail_review_list.detail_review_list');
    }

}
