<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests\CreateArticleRequest;

class ArticlesApiController extends Controller
{

    public function store(CreateArticleRequest $request)
    {
        $article = Article::create($request->all());
        return response()->json($article, 201);
    }
}
