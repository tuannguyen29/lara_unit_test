<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\DB;

class Cemetery extends Eloquent
{
    protected $fillable = ['CEMETERY_NAME'];

    public function getListCemetery()
    {
        return $listCemeteries = DB::table('cemeteries')->get();
    }
}
